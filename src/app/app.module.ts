import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { InfoComponent } from './main/info/info.component';
import { ActionsComponent } from './main/actions/actions.component';
import { DevelopersService } from './developers.service';

@NgModule({
  declarations: [
    AppComponent,
    InfoComponent,
    ActionsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [DevelopersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
