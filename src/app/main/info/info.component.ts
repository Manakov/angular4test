import { Component, OnInit } from '@angular/core';
import { DevelopersService } from '../../developers.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  name: string;
  age: number;
  job: string;

  constructor(private developersService: DevelopersService) {
    this.developersService.currentDeveloper$.subscribe((developer) => {
      this.name = developer.name;
      this.age = developer.age;
      this.job = developer.job;
    });
  }

  ngOnInit() {

  }
}
