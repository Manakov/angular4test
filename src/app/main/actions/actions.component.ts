import { Component, OnInit } from '@angular/core';
import { DevelopersService } from '../../developers.service';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {
  constructor(private developersService: DevelopersService) { }

  ngOnInit() {
    this.developersService.selectItem('lead');
  }

  selectItem(itemLevel: string) {
    this.developersService.selectItem(itemLevel);
  }
}
