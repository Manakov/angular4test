import {Developer} from './developer';
import { Subject } from 'rxjs/Subject';

export class DevelopersService {

  private data: Developer[] = [
    {
      name: 'Dmitry Kashuba',
      age: 30,
      job: 'Lead JavaScript Developer'
    },
    {
      name: 'John Dou',
      age: 25,
      job: 'Senior Front-End Developer'
    }
  ];

  private _currentDeveloper = new Subject<Developer>();

  currentDeveloper$ = this._currentDeveloper.asObservable();

  selectItem(itemLevel: string) {
    let currentDeveloperIndex = 0;

    switch (itemLevel) {
      case 'lead':
        currentDeveloperIndex = 0;
        break;

      case 'junior':
        currentDeveloperIndex = 1;
        break;
    }

    this._currentDeveloper.next(this.data[currentDeveloperIndex]);
  }
}
